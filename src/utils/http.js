import service from "./service"
import {parseInfo} from "./parseInfo"
export function get(url,params){
	const config={
		method:"get",
		url:url
	}	
	if(params){config.params=params}
	return service(config)
}

export function post(url,params){
	const config={
		method:"post",
		url:url
	}
	if(params){config.data=params}
	return service(config)
}

export function getToken(){
	return parseInfo()?parseInfo().token:''
}