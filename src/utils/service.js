import axios from "axios"
import {Message} from "element-ui"
import {getToken} from "./http.js"

const service =  axios.create({
	baseURL:'localhost:8080',
	timeout:5000	
})

// 请求拦截
service.interceptors.request.use(config => {
	console.log(999,config)
	if(getToken()){
		config.headers.token=getToken()
	}
	return config
},error =>{
	return Promise.reject(error)
})

// 响应拦截
service.interceptors.response.use(res => {
	const data=res.data
	if(data.code!=200){
		Message({
			type:"error",
			message:data.message
		})
		return Promise.reject(data.message)
	}else{
		return data
	}
	// return res.data
},error =>{
	return Promise.reject(error)
})
export default service
