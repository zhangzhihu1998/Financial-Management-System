import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		userinfo:{},
		rowData:{}
	},
	mutations: {
		updateData(state,payload){
			state.rowData=payload
		},
		saveuserinfo(state,payload){
			state.userinfo=payload;
			sessionStorage.setItem('userinfo',JSON.stringify(payload))
		}
	},
	actions: {
	},
	modules: {
	}
})
