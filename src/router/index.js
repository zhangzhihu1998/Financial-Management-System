import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '@/views/Login.vue'
import {getToken} from "@/utils/http"
Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: login
  },
  {
    path: '/',
    name: 'Layout',
    component: () => import('@/views/Layout.vue'),
	redirect:"/index",
	children:[
		{
			path: '/index',
			name: 'Index',
			meta:{roles:['admin']},
			component: () => import('@/views/index.vue')
		},
		{
			path: '/account/all',
			name: 'account_all',
			component: () => import('@/views/account/all.vue'),
			meta:{
				bread:["账户管理","所有人员"],
				roles:['user']
			}
		},
		{
			path: '/account/business',
			name: 'account_business',
			component: () => import('@/views/account/business.vue'),
			meta:{
				bread:["账户管理","业务人员"],
				roles:['admin']
			}
		},
		{
			path: '/account/audit',
			name: 'account_audit',
			component: () => import('@/views/account/audit.vue'),
			meta:{
				bread:["账户管理","审核人员"],
				roles:['admin']
			}
		},
		{
			path: '/account/risk',
			name: 'account_risk',
			component: () => import('@/views/account/risk.vue'),
			meta:{
				bread:["账户管理","风控经理"],
				roles:['admin']
			}
		},
		{
			path: '/product/all',
			name: 'product_all',
			component: () => import('@/views/product/all.vue'),
			meta:{
				bread:["产品管理","所有产品"],
				roles:['admin']
			}
		},
		{
			path: '/product/details',
			name: 'product_details',
			meta:{
				roles:['admin'],
			},
			component: () => import('@/views/product/details.vue')
		},
		{
			path: '/customer/info',
			name: 'customer_info',
			component: () => import('@/views/customer/info.vue'),
			meta:{
				bread:["客户管理","基本信息"],
				roles:['user']
			}
		},
		{
			path:"/orders/all",
			name:"orders_all",
			meta:{
				bread:["订单管理","所有订单"],
				roles:['admin']
			},
			component:()=>import("../views/orders/all.vue")
		},
		{
			path: '/todo',
			name: 'Todo',
			meta:{
				roles:['admin'],
			},
			component: () => import('@/views/todo.vue')
		},
		{
			path: '/my',
			name: 'My',
			meta:{
				roles:['admin']
			},
			component: () => import('@/views/my.vue')
		},
		{
			path: '/test',
			name: 'Test',
			meta:{
				roles:['ad']
			},
			component: () => import('@/views/test.vue')
		},
	]
  },
  {
	path:'*',
	name:'404',
	component: () => import("@/views/404.vue")
  }
]

const router = new VueRouter({
  mode:"history",
  routes
})

router.beforeEach((to,from,next)=>{
	if(getToken()){		
		if(to.path=='/login'){
			next(from.path)
		}else{
			// if(to.meta.roles){
			// 	if(to.meta.roles.includes('admin')){
			// 		next()
			// 	}else{
			// 		alert(404)
			// 		next('/404')
			// 	}		
			// }else{
			// 	next()
			// }
			next()
		}
		
	}else{
		if(to.path=='/login'){
			next()
		}else{
			next('/login')
		}
	}
	
	// console.log(45,to.meta.roles.includes('user'))
})
export default router
