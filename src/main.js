import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUi from "element-ui"
import 'element-ui/lib/theme-chalk/index.css'
import "./mock/mock.js"
import '../common.css'
import * as echarts from 'echarts';
Vue.prototype.$echarts= echarts
Vue.use(ElementUi);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
